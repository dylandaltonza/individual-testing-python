import unittest
from unittest import mock
from unittest.mock import patch, MagicMock
import builtins

from CustomerDatabaseMapping import CustomerDatabaseMapping
from ReadCSVFile import ReadCSVFile

#This is the Stub test
class TestCustomerDatabaseMapping(unittest.TestCase):

    # to run the test, In the command line, cd into the src directory, then enter the below command:
    # python -m unittest test_CustomerDatabaseMapping.py -v
    @mock.patch('ReadCSVFile.getFileData', create=True)
    def test_getCustomerDataFromFile(self, mock_getFile):
        dbSetup = MagicMock('DB')
        myClass = CustomerDatabaseMapping(dbSetup)
        mock_getFile.return_value = 'Here is the dummy data'
        result = myClass.getCustomerDataFromFile()
        self.assertEqual('Here is the dummy data', result)
        


    def main():
        unittest.main()

    if __name__ == "__main__":
        unittest.main()



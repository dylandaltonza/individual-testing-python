import io
from io import StringIO
import unittest
from unittest import mock
from unittest.mock import MagicMock
import builtins
import csv

from ReadCSVFile import ReadCSVFile
from ReadCSVStub import ReadCSVStub

#The tests in this file are a Stub and a Mock
class TestReadCSVFile(unittest.TestCase):

    # To run this file's tests, navigate to the src folder in Command line, then type the below command:
    # python -m unittest test_ReadCSVFile.py -v
    CSVStub = ReadCSVStub()

    def test_getCustomerDataFromStub(self):
        data = self.CSVStub.getData()
        self.assertEqual(['mocktest@email.com', 'mock', 'test', 'testpassword'], data)

    def test_getFileData(self):
        readFile = ReadCSVFile()
        readFile.getFileData = MagicMock(return_value=[['mocktest@email.com', 'mock', 'test', 'testpassword']])
        result = readFile.getFileData()
        self.assertEqual( [['mocktest@email.com', 'mock', 'test', 'testpassword']], result)

    def main():
        unittest.main()

    if __name__ == "__main__":
        unittest.main()
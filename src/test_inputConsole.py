import io
from io import StringIO
import unittest
from unittest import mock
from unittest.mock import MagicMock
import builtins

from InputConsole import InputConsole
from WriteToFile import WriteToFile

class TestInputConsole(unittest.TestCase):

    def test_getInputString(self):
        fakeFile = io.StringIO("Here is some dummy data")
        mobject = MagicMock(InputConsole)

        write = WriteToFile()
        write.writeToFile = MagicMock(return_value=fakeFile)
        result = InputConsole.getInputString(mobject, 'Hello')
        self.assertEqual('Hello', result)

    def main():
        unittest.main()

    if __name__ == "__main__":
        unittest.main()
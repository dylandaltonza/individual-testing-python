import io
from io import StringIO
import unittest
from unittest import mock
from unittest.mock import MagicMock, mock_open
import builtins

from WriteToFile import WriteToFile

#This is the Fake test
class TestWriteToFile(unittest.TestCase):
    
    def test_writeToFile(self):
        read_data = 'dummy data'
        mock_open = mock.mock_open(read_data=read_data)

        with mock.patch('builtins.open', mock_open):
            result = WriteToFile.writeToFile("userOutputLog.csv")
        self.assertEqual('dummy data', result)
    
    def main():
        unittest.main()

    if __name__ == "__main__":
        unittest.main()

    